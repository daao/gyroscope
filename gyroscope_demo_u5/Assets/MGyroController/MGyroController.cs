﻿using UnityEngine;
using System.Collections;

public class MGyroController : MonoBehaviour {

	// Use this for initialization
	void Start () {

        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = true;
        }
	}

    void Update()
    {
        transform.rotation =
            GyroToUnity(Input.gyro.attitude);
            //Input.gyro.attitude;
    }

    private static Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }
}
